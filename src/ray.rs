use crate::vec3::{dot, Vec3};

pub type Point = Vec3;
pub type Direction = Vec3;

pub struct Ray {
    pub origin: Point,
    pub direction: Direction,
}

pub fn hit_sphere(centre: Point, radius: f64, ray: &Ray) -> bool {
    let oc: Direction = ray.origin - centre;
    let a = dot(ray.direction, ray.direction);
    let b = 2.0 * dot(oc, ray.direction);
    let c = dot(oc, oc) - radius * radius;
    let discriminant = b * b - 4.0 * a * c;
    return discriminant > 0.0;
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::vec3::vec3;

    #[test]
    fn test_hit_sphere() {
        let ray = Ray {
            origin: vec3(0.0, 0.0, 0.0),
            direction: vec3(1.0, 1.0, 1.0),
        };
        let centre = vec3(0.0, 0.0, 1.0);
        assert_eq!(hit_sphere(centre, 0.0, &ray), false);
        assert_eq!(hit_sphere(centre, 1.0, &ray), true);
        assert_eq!(hit_sphere(centre, 2.0, &ray), true);
    }
}
