use crate::vec3::Vec3;

pub struct Image {
    pub width: u16,
    pub height: u16,
    pub pixels: Vec<Vec3>,
}

// Here the vector a is assumed to have (x, y, z) as
// a representation of r, g, b color space between 0 and 1
fn print_color(a: Vec3) -> String {
    let r = (255.999 * a.x).floor() as u8;
    let g = (255.999 * a.y).floor() as u8;
    let b = (255.999 * a.z).floor() as u8;
    return format!("{} {} {}", r, g, b);
}

pub fn print_ppm(image: Image) -> Result<Vec<String>, &'static str> {
    if (image.width as u32) * (image.height as u32) != image.pixels.len() as u32 {
        return Err("image length doesn't match dimensions, is it ready to print?");
    }

    let mut output = vec![
        "P3".into(),
        format!("{} {}", image.width, image.height),
        "255".into(),
    ];

    for pixel in &image.pixels {
        output.push(print_color(*pixel));
    }

    return Ok(output);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_print_ppm_result() {
        let image = Image {
            width: 1,
            height: 1,
            pixels: vec![Vec3 {
                x: 0.0,
                y: 0.33,
                z: 1.0,
            }],
        };
        assert_eq!(
            print_ppm(image).unwrap(),
            vec!["P3", "1 1", "255", "0 84 255"]
        );
    }

    #[test]
    fn test_print_ppm_validation() {
        assert!(print_ppm(Image {
            width: 100,
            height: 100,
            pixels: vec![Vec3 {
                x: 0.0,
                y: 0.0,
                z: 0.0
            }]
        })
        .is_err());
    }
}
