use std::ops::{Add, Div, Mul, Sub};

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct Vec3 {
    pub x: f64,
    pub y: f64,
    pub z: f64,
}

pub fn vec3(x: f64, y: f64, z: f64) -> Vec3 {
    return Vec3 { x, y, z };
}

fn length_squared(a: Vec3) -> f64 {
    return a.x * a.x + a.y * a.y + a.z * a.z;
}

fn length(a: Vec3) -> f64 {
    return f64::sqrt(length_squared(a));
}

impl Add for Vec3 {
    type Output = Self;

    fn add(self, rhs: Self) -> Self {
        vec3(self.x + rhs.x, self.y + rhs.y, self.z + rhs.z)
    }
}

impl Sub for Vec3 {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self {
        vec3(self.x - rhs.x, self.y - rhs.y, self.z - rhs.z)
    }
}

impl Mul<f64> for Vec3 {
    type Output = Self;

    fn mul(self, rhs: f64) -> Self {
        vec3(self.x * rhs, self.y * rhs, self.z * rhs)
    }
}

impl Div<f64> for Vec3 {
    type Output = Self;

    fn div(self, rhs: f64) -> Self {
        self * (1.0 / rhs)
    }
}

pub fn dot(a: Vec3, b: Vec3) -> f64 {
    return a.x * b.x + a.y * b.y + a.z * b.z;
}

fn cross(a: Vec3, b: Vec3) -> Vec3 {
    return vec3(
        a.y * b.z - a.z * b.y,
        a.z * b.x - a.x * b.z,
        a.x * b.y - a.y * b.x,
    );
}

pub fn unit_vector(a: Vec3) -> Vec3 {
    return a / length(a);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_add() {
        assert_eq!(
            vec3(1.0, 2.0, 3.0) + vec3(4.0, 5.0, 6.0),
            vec3(5.0, 7.0, 9.0)
        );
    }

    #[test]
    fn test_subtract() {
        assert_eq!(
            vec3(1.0, 2.0, 3.0) - vec3(4.0, 5.0, 6.0),
            vec3(-3.0, -3.0, -3.0)
        );
    }

    #[test]
    fn test_scale() {
        assert_eq!(vec3(1.0, 2.0, 3.0) * 2.0, vec3(2.0, 4.0, 6.0));
    }

    #[test]
    fn test_dot() {
        assert_eq!(dot(vec3(1.0, 2.0, 3.0), vec3(1.0, 1.0, 1.0)), 6.0);
    }

    #[test]
    fn test_cross() {
        assert_eq!(
            cross(vec3(1.0, 2.0, 3.0), vec3(1.0, 1.0, 1.0)),
            vec3(-1.0, 2.0, -1.0)
        );
    }

    #[test]
    fn test_length_squared() {
        assert_eq!(length_squared(vec3(3.0, 4.0, 1.0)), 26.0);
    }

    #[test]
    fn test_length() {
        assert_eq!(length(vec3(3.0, 4.0, 0.0)), 5.0);
        assert_eq!(length(vec3(3.0, 0.0, 4.0)), 5.0);
        assert_eq!(length(vec3(4.0, 0.0, 3.0)), 5.0);
    }

    #[test]
    fn test_unit_vector() {
        assert_eq!(unit_vector(vec3(3.0, 0.0, 0.0)), vec3(1.0, 0.0, 0.0));
    }
}
