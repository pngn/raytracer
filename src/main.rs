use ray::hit_sphere;
use ray::Point;
use ray::Ray;
use vec3::unit_vector;
use vec3::vec3;
use vec3::Vec3;

mod ppm;
mod ray;
mod vec3;

fn main() {
    // the dimensions of the image that we want to produce
    let aspect_ratio = 16.0 / 9.0;
    let width = 400;
    let height = (width as f64 / aspect_ratio) as u16;

    // define the dimensions of the viewport we want to model for the camera
    let viewport_height = 2.0;
    let viewport_width = aspect_ratio * viewport_height;
    let focal_length = 1.0;

    // the position of the camera
    let origin = Point {
        x: 0.0,
        y: 0.0,
        z: 0.0,
    };

    // vectors to represent the full width of the viewport
    let horizontal = vec3(viewport_width, 0.0, 0.0);
    let vertical = vec3(0.0, viewport_height, 0.0);
    let forward = vec3(0.0, 0.0, focal_length);

    // calculate the position of the bottom left corner of the viewport
    let lower_left_corner = origin - (horizontal / 2.0) - (vertical / 2.0) - forward;

    let mut image = ppm::Image {
        width,
        height,
        pixels: Vec::new(),
    };

    let mut rays: Vec<Ray> = Vec::new();

    // start our image coordinate system from the bottom-left
    for j in (0..image.height).rev() {
        for i in 0..image.width {
            // map positions in the image to positions in the viewport
            let u = horizontal * (i as f64 / image.width as f64);
            let v = vertical * (j as f64 / image.height as f64);

            // construct a ray from the camera origin to the point on the viewport
            rays.push(Ray {
                origin,
                // subtract the origin to make it relative from the ray start
                direction: lower_left_corner + u + v - origin,
            });
        }
    }

    let sphere_centre = origin - forward / 2.0;
    let radius = 0.2;

    for ray in rays {
        if hit_sphere(sphere_centre, radius, &ray) {
            image.pixels.push(vec3(1.0, 1.0, 0.0));
            continue;
        }

        let unit_direction: Vec3 = unit_vector(ray.direction);
        // the y direction is now somewhere between -1 and 1
        // shift the value to be between 0 and 1
        let t = 0.5 * (unit_direction.y + 1.0);

        let white = vec3(1.0, 1.0, 1.0);
        let blue = vec3(0.2, 0.7, 1.0);

        // blend between white and blue based on the value of t
        image.pixels.push(white * (1.0 - t) + blue * t);
    }

    let lines = ppm::print_ppm(image).unwrap();

    for line in lines {
        println!("{}", line);
    }
}
