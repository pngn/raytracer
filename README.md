# raytracer

Following along with the ray tracing in a weekend course.

https://raytracing.github.io/

```sh
cargo run --quiet > image.ppm
```
